from django.conf.urls import patterns, url
from userprofile import views

urlpatterns = patterns('',
    url(r'^register-api/$', views.RegistrationApi.as_view(), name='register-api'),
    url(r'^login-api/$', views.LoginApi.as_view(), name='login-api'),
)