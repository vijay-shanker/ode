from django.contrib.auth import get_user_model
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import RegistrationSerializer, LoginSerializer
from rest_framework.permissions import IsAuthenticated

User = get_user_model()


class RegistrationApi(APIView):
    '''
    API to register new User
    '''
    serializer_class = RegistrationSerializer

    def get_serializer_class(self):
        return self.serializer_class

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            # create user object if serializer is valid
            user_obj = User.objects.create(**serializer.data)
            # set password to convert password into hash
            user_obj.set_password(serializer.data['password'])
            token_obj, created = Token.objects.get_or_create(user=user_obj)
            user_obj.save()
            return Response({'token': token_obj.key,
                             'username': user_obj.username}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LoginApi(APIView):
    '''
    Login APi - pass email and password to login
    return token and username
    '''
    serializer_class = LoginSerializer

    def get_serializer_class(self):
        return self.serializer_class

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            try:
                user_obj = User.objects.get(email=serializer.data['email'])
            except:
                return Response({'user does not exist'}, status=status.HTTP_400_BAD_REQUEST)

            if user_obj.check_password(serializer.data['password']):
                token, created = Token.objects.get_or_create(user=user_obj)

                return Response({'token': token.key,
                                 'username': user_obj.username}, status=status.HTTP_200_OK)
            return Response({'username password not matched'}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
