from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import User
User = get_user_model()

class RegistrationSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username','email', 'password')

    def validate_email(self, value):
        user = User.objects.filter(email=value)
        if user:
            raise serializers.ValidationError("User with this email already exists.")
        else:
            return value

class LoginSerializer(serializers.ModelSerializer):

    email = serializers.EmailField(required=True) #To override model field ie to prevent save() call on post method
    class Meta:
        model = User
        fields =('email', 'password')
