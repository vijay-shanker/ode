/*-------------------------------------------------------------------------------------------------------------------------------*/
/* 	THEME NAME 		: Square - Admin Template
* 	THEME VERSION	: 1.0.0
*	URL				: https://twitter.com/naztemplate
*	AUTHOR			: naztemplate.com
*	LAST UPDATE		: 10/11/2014
* 	COPYRIGHT		: Copyright 2014 All Rights Reserved. | by Square
*/
/*-------------------------------------------------------------------------------------------------------------------------------*/

(function( $ ) {

	'use strict';

	var initMap = function( $el, options ) {
		var defaults = {
			backgroundColor: null,
			color: '#FFFFFF',
			hoverOpacity: 0.7,
			selectedColor: '#FF7373',
			enableZoom: true,
			borderWidth:1,
			showTooltip: true,
			values: sample_data,
			scaleColors: ['#1AA2E6', '#0088CC'],
			normalizeFunction: 'polynomial'
		};

		$el.vectorMap( $.extend( defaults, options ) );
	};

	$(function() {
		$( '[data-vector-map]' ).each(function() {
			var $this = $(this);
			initMap( $this, ($this.data( 'plugin-options' ) || {}) )
		});
	});

}).apply( this, [ jQuery ]);