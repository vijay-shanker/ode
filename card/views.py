import os
from random import randint
from django.conf import settings
from django.http import JsonResponse

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser,JSONParser, FormParser

from .models import *
from .serializers import *


class CardViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = CardSerializer
    queryset = Card.objects.all()
    
class UploadCardView(generics.GenericAPIView):
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser, FormParser, JSONParser)
    
    def post(self, request, *args, **kwargs):
        cardId = request.POST.get('cardId')   
        card={}
        card['title'] = request.POST['title']
        card['status'] = request.POST['status']
        card['desc'] = request.POST['desc']
        card['profile_name'] = request.POST['profile_name']
        
        if request.POST['is_draft'] == 'true':
            card['is_draft'] = True
        else:
            card['is_draft'] = False
        
        if request.POST.get('profile_image', None):
            card['profile_image'] = request.POST['profile_image']
        if request.POST.get('cover_image', None):
            card['cover_image'] = request.POST['cover_image']
        if request.POST.get('profile_image', None):
            card['card_image'] = request.POST['card_image']
        card['youtube_playlist'] = request.POST['youtube_playlist']

        if cardId:
            card['updated_by'] = request.user.email
            Card.objects.filter(pk=cardId).update(**card)
            card = Card.objects.get(pk=cardId)
        else:
            card['added_by'] = request.user.email
            card = Card.objects.create(**card)
        
        social = {}
        social['link_type'] = 'Youtube'
        social['link'] = request.POST['youtube_link']
        if cardId:
            SocialSettings.objects.filter(
                link_type='Youtube', card_id=cardId
            ).update(**social)
        else:
            social['card_id'] = card.id
            SocialSettings.objects.create(**social)
            
        social = {}
        social['link_type'] = 'Instagram'
        social['link'] = request.POST['insta_link']
        if cardId:
            SocialSettings.objects.filter(
                link_type='Instagram', card_id=cardId
            ).update(**social)
        else:
            social['card_id'] = card.id
            SocialSettings.objects.create(**social)

            
        social = {}
        social['link_type'] = 'Twitter'
        social['link'] = request.POST['twitter_link']
        if cardId:
            SocialSettings.objects.filter(
                link_type='Twitter', card_id=cardId
            ).update(**social)
        else:
            social['card_id'] = card.id
            SocialSettings.objects.create(**social)

        
        social = {}
        social['link_type'] = 'Facebook'
        social['link'] = request.POST['fb_link']
        if cardId:
            SocialSettings.objects.filter(
                link_type='Facebook', card_id=cardId
            ).update(**social)
        else:
            social['card_id'] = card.id
            SocialSettings.objects.create(**social)
        
        YoutubeLinks.objects.filter(card=card).delete()
        youtube = {}
        youtube['card_id'] = card.id
        youtube['link'] = request.POST['youtube_video_1']
        YoutubeLinks.objects.create(**youtube)
        
        youtube = {}
        youtube['card_id'] = card.id
        youtube['link'] = request.POST['youtube_video_2']
        YoutubeLinks.objects.create(**youtube)
        
        youtube = {}
        youtube['card_id'] = card.id
        youtube['link'] = request.POST['youtube_video_3']
        YoutubeLinks.objects.create(**youtube)
        
        return Response({'success_flag':True}, status=status.HTTP_200_OK)
        
        
class UploadPicView(generics.GenericAPIView):
    def post(self, request, *args, **kwargs):
        image_file = request.FILES['file']
        image_type = request.POST['image_type']
        if 'cardId' in request.POST:
            card = Card.objects.get(pk=request.POST['cardId'])
            if image_type == 'card-image':
                card.card_image = image_file
                card.save()
                return JsonResponse({'image_type':image_type, 'image_path':card.card_image.url})
                
        destination =  '/'.join([settings.MEDIA_ROOT, image_type, image_file.name])
        image_path = '/'.join(['/media', image_type, image_file.name])
        
        if os.path.exists(destination):
            filename, extension = os.path.splitext(image_file.name)
            ran_str =  str(randint(100,999))
            filename = filename + ran_str + extension
            
            destination = '/'.join([
                settings.MEDIA_ROOT, 
                image_type, filename ])
            image_path = '/'.join(['/media', image_type, filename])
            
        with open(destination, 'wb+') as desti:
            for chunk in image_file.chunks():
                desti.write(chunk)
        
        return JsonResponse({'image_type':image_type, 'image_path':image_path})
    
    
class UploadedCardView(generics.ListAPIView):
    serializer_class = CardPreviewSerializer
    
    def get_queryset(self):
        if self.kwargs['status'] == 'public':
            return Card.objects.filter(status='public')
        elif self.kwargs['status'] == 'private':
            return Card.objects.filter(status='private')
        elif self.kwargs['status'] == 'draft':
            return Card.objects.filter(is_draft=True)
        elif self.kwargs['status'] == 'published':
            return Card.objects.exclude(is_draft=False)
        else:
            return Card.objects.all()
        

class UploadedCardViewById(generics.ListAPIView):
    serializer_class = CardPreviewSerializer

    def get_queryset(self):
        pk = self.kwargs['card_id']
        return Card.objects.filter(id=pk)

class GetSocialSettingsView(generics.ListAPIView):
    # permission_classes = (IsAuthenticated,)
    serializer_class = SocialSettingsSerializer
    
    def get_queryset(self):
        pk = self.kwargs['card_id']
        card = Card.objects.get(pk=pk)
        social_obj = SocialSettings.objects.filter(card=card)
        return social_obj
    
    
class GetYoutubeLinksView(generics.ListAPIView):
    #permission_classes = (IsAuthenticated,)
    serializer_class = YoutubeLinksSerializer
    
    def get_queryset(self):
        pk = self.kwargs['card_id']
        card = Card.objects.get(pk=pk)
        video_obj = YoutubeLinks.objects.filter(card=card)
        return video_obj    
                
    
