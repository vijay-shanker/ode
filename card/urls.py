from rest_framework.routers import DefaultRouter
from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^upload-card/$', UploadCardView.as_view(), name="upload_card" ),
    url(r'^upload-pic/$', UploadPicView.as_view(), name="upload_pic" ),
    url(r'^retrieve-cards/(?P<status>\w+)/$', UploadedCardView.as_view(), name="retrieve_cards"),
    url(r'^retrieve-cards-by-id/(?P<card_id>\w+)/$', UploadedCardViewById.as_view(), name="retrieve_cards_by_id"),
    url(r'^get-social-data/(?P<card_id>\d+)/$', GetSocialSettingsView.as_view(), name="get_social_settings"),
    url(r'^get-youtube-data/(?P<card_id>\d+)/$', GetYoutubeLinksView.as_view(), name="get_youtubelinks"),
]

router = DefaultRouter()
router.register(r'card', CardViewSet)
urlpatterns += router.urls