from rest_framework import serializers
from .models import *
from django.template.defaultfilters import slugify


class CardPreviewSerializer(serializers.ModelSerializer):

    slug = serializers.SerializerMethodField()
    class Meta:
        model = Card
        fields = ('id', 'profile_image', 'title', 'desc', 'status', 'profile_image', 'card_image', 'cover_image',
                  'youtube_playlist', 'slug')

    def get_slug(self, obj):
        return slugify(obj.title)
        

class CardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Card
        exclude = ()
    
    
class SocialSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = SocialSettings
        exclude = ()
        
        
class YoutubeLinksSerializer(serializers.ModelSerializer):
    class Meta:
        model = YoutubeLinks
        fields = ('link',)