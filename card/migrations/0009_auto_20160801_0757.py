# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('card', '0008_auto_20160729_0852'),
    ]

    operations = [
        migrations.AddField(
            model_name='card',
            name='added_by',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='card',
            name='added_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 8, 1, 7, 57, 5, 314487, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='card',
            name='updated_by',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='card',
            name='updated_on',
            field=models.DateTimeField(default=datetime.datetime(2016, 8, 1, 7, 57, 12, 538456, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
