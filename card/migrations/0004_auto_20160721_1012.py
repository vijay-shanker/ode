# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-07-21 10:12
from __future__ import unicode_literals

import card.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('card', '0003_auto_20160721_1007'),
    ]

    operations = [
        migrations.AlterField(
            model_name='socialsettings',
            name='link_icon',
            field=models.ImageField(null=True, upload_to=card.models.icon_upload),
        ),
    ]
