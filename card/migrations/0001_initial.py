# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-07-19 13:50
from __future__ import unicode_literals

import card.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Card',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=100, null=True)),
                ('profile_name', models.CharField(max_length=100)),
                ('desc', models.TextField()),
                ('status', models.CharField(choices=[('Public', 'Public'), ('Private', 'Private'), ('Draft', 'Draft')], max_length=10)),
                ('profile_image', models.ImageField(upload_to=card.models.profile_image_upload)),
                ('card_image', models.ImageField(upload_to=card.models.card_image_upload)),
                ('cover_image', models.ImageField(upload_to=card.models.cover_image_upload)),
                ('youtube_playlist', models.URLField()),
            ],
        ),
        migrations.CreateModel(
            name='SocialSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('link_type', models.CharField(choices=[('Facebook', 'Facebook'), ('Instagram', 'Instagram'), ('Twitter', 'Twitter'), ('Youtube', 'Youtube')], max_length=20)),
                ('link_icon', models.ImageField(upload_to=card.models.icon_upload)),
                ('link', models.URLField()),
                ('card', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='card.Card')),
            ],
        ),
        migrations.CreateModel(
            name='YoutubeLinks',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('link', models.URLField()),
                ('card', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='card.Card')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='socialsettings',
            unique_together=set([('link_type', 'card')]),
        ),
    ]
