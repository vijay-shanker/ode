from __future__ import unicode_literals

from django.db import models

# Create your models here.
def card_image_upload(instance, filename):
    return '/'.join(['card-image', filename])

def profile_image_upload(instance, filename):
    return '/'.join(['profile-image', filename])

def cover_image_upload(instance, filename):
    return '/'.join(['cover-image', filename])

def icon_upload(instance, filename):
    return '/'.join(['icon-image', filename])


class Card(models.Model):
    CARD_STATUS = (
        ('public', 'Public'),
        ('private', 'Private'),
    )

    title = models.CharField(max_length=100, null=True, blank=True)
    profile_name = models.CharField(max_length=100, null=True, blank=True)
    desc = models.TextField()
    status = models.CharField(max_length=10, choices=CARD_STATUS)
    profile_image = models.CharField(max_length=200, null=True)
    card_image = models.CharField(max_length=200, null=True)
    cover_image = models.CharField(max_length=200, null=True)
    youtube_playlist = models.URLField(max_length=200)
    is_draft = models.BooleanField(default=False)
    added_by = models.CharField(max_length=100, null=True, blank=True)
    added_on = models.DateTimeField(auto_now_add=True)
    updated_by = models.CharField(max_length=100, null=True, blank=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class SocialSettings(models.Model):
    LINK_TYPE = (
        ('Facebook', 'Facebook'),
        ('Instagram', 'Instagram'),
        ('Twitter', 'Twitter'),
        ('Youtube', 'Youtube')
    )
    card = models.ForeignKey('card.Card')
    link_type = models.CharField(max_length=20, choices=LINK_TYPE)
    link_icon = models.ImageField(upload_to=icon_upload, null=True)
    link = models.URLField(max_length=200)

    class Meta:
        unique_together = ('link_type', 'card' )
        verbose_name_plural = ('Social Settings')

    def __str__(self):
        return self.card.title


class YoutubeLinks(models.Model):
    card = models.ForeignKey('card.Card')
    link = models.URLField(max_length=200)

    class Meta:
        verbose_name_plural = ('Youtube Links')

    def __str__(self):
        return self.card.title




