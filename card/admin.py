from django.contrib import admin
from .models import Card, SocialSettings, YoutubeLinks
# Register your models here.
admin.site.register([Card, SocialSettings, YoutubeLinks])