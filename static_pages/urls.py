from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
    url(r'^page-api/(?P<page_id>\d)/$', views.PageAPI.as_view(), name='page-api'),

    # url(r'^up-page-api', views.PageAPI.as_view(), name='get-page-api'),
)