from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Page(models.Model):
    name = models.CharField(_('Page Name'), max_length=50, null=True, blank=True)
    title = models.CharField(_('Title'), max_length=100, null=True, blank=True)
    description = models.TextField(_('Description'), null=True, blank=True)

    def __str__(self):
        return self.name