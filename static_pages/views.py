from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, BasePermission
from .models import Page
from .serializers import PageSerializer


SAFE_METHODS = ['GET']
class IsAuthenticatedOrReadOnly(BasePermission):
    """
    The request is authenticated as a user, or is a read-only request.
    """

    def has_permission(self, request, view):
        if (request.method in SAFE_METHODS or request.user and request.user.is_authenticated()):
            return True
        return False


class PageAPI(APIView):
    '''
    API to get and edit Pages.
     Page objects are created in Page admin
    '''

    serializer_class = PageSerializer
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get_serializer_class(self):
        return self.serializer_class

    # get page id as input and return page data
    def get(self, request, *args, **kwargs):
        page_id = kwargs['page_id']
        page_data = Page.objects.filter(id = page_id)
        page_serializer = self.serializer_class(page_data, many=True)
        return Response(page_serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            try:
                page_obj = Page.objects.filter(id=kwargs['page_id'])
            except:
                return Response({'Page does not exist'}, status=status.HTTP_400_BAD_REQUEST)
            page_obj.update(**serializer.data)
            return Response({'update successful'}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


