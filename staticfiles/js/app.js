var app = angular.module('myApp', ['ngRoute']);

app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('//');
    $interpolateProvider.endSymbol('//');
  });

app.config(function($routeProvider, $locationProvider){
	$routeProvider
	.when('/', {
		templateUrl: '/static/templates/login.html',
		controller : 'LoginController'
	})
	.when('/dashboard/', {
		templateUrl: '/static/templates/new_card_upload.html',
		controller: 'cardUploadCtrl'
	})
	.when('/dashboard/edit-card/:pk/', {
		templateUrl: 'static/templates/edit_card.html',
		controller: 'editCardController'
	})
	.when('/dashboard/uploaded-cards/', {
		templateUrl: '/static/templates/uploaded_cards.html',
		controller:  'uploadedCardCtrl'
	})
	.when('/dashboard/published-cards/', {
		templateUrl : '/static/templates/published_cards.html',
		controller: 'publishedCardCtrl'
	})
	.when('/dashboard/private-cards/', {
		templateUrl : '/static/templates/private_cards.html',
		controller: 'privateCardCtrl'
	})
	.when('/register/', {
        templateUrl : '/static/templates/register.html',
        controller : 'SignUpController',
    })
    .when('/dashboard/pages/',{
        templateUrl : '/static/templates/who_we_are.html',
        controller : 'PageController',
    })
    .when('/logout/',{
        templateUrl : '',
        controller : 'logOutController'
    })
	.otherwise({'redirectTo':'/'});
	
});


app.controller('editCardController', function($scope, $routeParams, $http, $window){
	// $scope.regex = RegExp('^((http|https?|ftp)://)?([a-z]+[.])?[a-z0-9-]+([.][a-z]{1,4}){1,2}(/.*[?].*)?$', 'i');
	$scope.username = localStorage.getItem('username');
	$scope.show_card_image= '/static/img/card200x283.jpg';
	$scope.show_profile_image= '/static/img/profile-215x215.jpg';
	$scope.show_cover_image= '/static/img/cover1500x248.jpg';
	var pk = $routeParams.pk;
	$scope.cardId = pk;
	var token = localStorage.getItem('token');
	var geturl = '/card/card/' + pk + '/';
	var headers = {headers: {'Authorization': 'Token '+token} };
	$http({
		 method: 'GET',
		 url: '/card/card/'+pk + '/',
		 headers:{'Authorization': 'Token ' + token},
		 data:{}
	}).success(function(data){
		$scope.title = data.title;
		$scope.profile_name = data.profile_name,
		$scope.desc = data.desc;
		$scope.status = data.status;
		$scope.show_card_image = data.card_image;
		$scope.show_cover_image = data.cover_image;
		$scope.show_profile_image = data.profile_image;
		$scope.youtube_playlist = data.youtube_playlist;

        // save data in local storage to prevent empty image save ++ correct it's path
        var card_image_path = '/media' + data.card_image.split('/media')[1]
		localStorage.setItem('card-image', card_image_path);

        var cover_image_path = '/media' + data.cover_image.split('/media')[1]
		localStorage.setItem('cover-image', cover_image_path);

        var profile_image_path = '/media' + data.profile_image.split('/media')[1]
		localStorage.setItem('profile-image', profile_image_path);
	});
	
	var get_social_url = '/card/get-social-data/' + pk + '/';
	$http({
		method:'GET',
		url: get_social_url,
		data:{},
		headers:{'Authorization': 'Token ' + token},
	}).success(function(data){
		for(var i=0; i<data.length; i++	){
			if (data[i].link_type == 'Facebook'){
				$scope.fb_link = data[i].link;
			}else if (data[i].link_type == 'Twitter'){
				$scope.twitter_link = data[i].link;
			}else if (data[i].link_type == 'Instagram'){
				$scope.insta_link = data[i].link;
			}else if (data[i].link_type == 'Youtube'){
				$scope.youtube_link = data[i].link;
			}
			
		}
	});
	
	var get_videos_url = '/card/get-youtube-data/' + pk + '/';
	$http({
		method:'GET',
		url: get_videos_url,
		data:{},
		headers:{'Authorization': 'Token ' + token},
	}).success(function(data){
		if(data.length==1){
			$scope.youtube_video_1 = data[0].link;
		}else if (data.length == 2){
			$scope.youtube_video_1 = data[0].link;
			$scope.youtube_video_2 = data[1].link;
		}else if (data.length==3){
			$scope.youtube_video_1 = data[0].link;
			$scope.youtube_video_2 = data[1].link;
			$scope.youtube_video_3 = data[2].link;	
		}
	});
	
	$scope.uploadFile = function(files, image_type){
		$scope.cardImage = files[0].name;
		console.log(files[0].name);
		var fd = new FormData();
		fd.append('cardId', pk);
		fd.append("file", files[0]);
		fd.append("image_type", image_type);
		
		$http.post('/card/upload-pic/', fd, {
	        withCredentials: false,
	        headers: {'Content-Type': undefined },
	        transformRequest: angular.identity
	    }).success(function(data){
	    	localStorage.setItem(data.image_type, data.image_path);
	    	if (data.image_type == 'card-image'){
	    		$scope.show_card_image = data.image_path;
	    	}else if(data.image_type == 'profile-image'){
	    		$scope.show_profile_image = data.image_path;
	    	}else if (data.image_type == 'cover-image'){
	    		$scope.show_cover_image = data.image_path;
	    	}
	    });
	};
	
	$scope.uploadCard = function(){
		var token = localStorage.getItem('token');
		var card_image = localStorage.getItem('card-image');
		var profile_image = localStorage.getItem('profile-image');
		var cover_image = localStorage.getItem('cover-image');
		var token = localStorage.getItem('token');
		console.log($scope.title);
		$http({
            method: 'POST',
            url: '/card/upload-card/',
            headers: {
    			'Content-Type': undefined,
                'Authorization': 'Token ' + token,
            },
            data: {
            	cardId:pk,
                title: $scope.title,
                profile_name:$scope.profile_name,
                desc: $scope.desc,
                status: $scope.status,
                card_image: card_image,
                cover_image:cover_image,
                profile_image:profile_image,
                fb_link: $scope.fb_link,
                insta_link: $scope.insta_link,
                twitter_link: $scope.twitter_link,
                youtube_link: $scope.youtube_link,
                youtube_video_1: $scope.youtube_video_1,
                youtube_video_2: $scope.youtube_video_2,
                youtube_video_3: $scope.youtube_video_3,
                youtube_playlist: $scope.youtube_playlist
            },
            transformRequest: function (data, headersGetter) {
                var formData = new FormData();
                angular.forEach(data, function (value, key) {
                    formData.append(key, value);
                });

                var headers = headersGetter();
                delete headers['Content-Type'];

                return formData;
            }
        })
        .success(function (data) {
			localStorage.removeItem('card-image');
			localStorage.removeItem('cover-image');
			localStorage.removeItem('profile-image');
			$window.location.href = '#/dashboard/edit-card/'+ pk +'/';
        })
        .error(function (data, status) {

        });
	};
	
	
});

app.controller("uploadedCardCtrl", function($scope,$http, $window){
	$scope.username = localStorage.getItem('username');
	var token = localStorage.getItem('token');
	var geturl = '/card/retrieve-cards/getall/';
	var headers = {headers: {'Authorization': 'Token '+token} };
	$http.get(geturl, headers).success(function(data, status, headers, config){
		$scope.cards = data;
	});
});


app.controller("publishedCardCtrl", function($scope,$http, $window){
	$scope.username = localStorage.getItem('username');
	var token = localStorage.getItem('token');
	var geturl = '/card/retrieve-cards/published/';
	var headers = {headers: {'Authorization': 'Token '+token} };
	$http.get(geturl, headers).success(function(data, status, headers, config){
		$scope.cards = data;
	});
});

app.controller("privateCardCtrl", function($scope,$http, $window){
	$scope.username = localStorage.getItem('username');
	var token = localStorage.getItem('token');
	var geturl = '/card/retrieve-cards/private/';
	var headers = {headers: {'Authorization': 'Token '+token} };
	$http.get(geturl, headers).success(function(data, status, headers, config){
		$scope.cards = data;
	});
});



app.controller("cardUploadCtrl", function($scope, $http, $window){
	// $scope.regex = RegExp('^((http|https?|ftp)://)?([a-z]+[.])?[a-z0-9-]+([.][a-z]{1,4}){1,2}(/.*[?].*)?$', 'i');
    $scope.username = localStorage.getItem('username');
	$scope.show_card_image= '/static/img/card200x283.jpg';
	$scope.show_profile_image= '/static/img/profile-215x215.jpg';
	$scope.show_cover_image= '/static/img/cover1500x248.jpg';
	
	$scope.uploadFile = function(files, image_type){
		$scope.cardImage = files[0].name;
		console.log(files[0].name);
		var fd = new FormData();

		fd.append("file", files[0]);
		fd.append("image_type", image_type);
		
		$http.post('/card/upload-pic/', fd, {
	        withCredentials: false,
	        headers: {'Content-Type': undefined },
	        transformRequest: angular.identity
	    }).success(function(data){
	    	localStorage.setItem(data.image_type, data.image_path);
	    	if (data.image_type == 'card-image'){
	    		$scope.show_card_image = data.image_path;
	    	}else if(data.image_type == 'profile-image'){
	    		$scope.show_profile_image = data.image_path;
	    	}else if (data.image_type == 'cover-image'){
	    		$scope.show_cover_image = data.image_path;
	    	}
	    });
	};
	
	$scope.uploadCard = function(){
		var token = localStorage.getItem('token');
		var card_image = localStorage.getItem('card-image');
		var profile_image = localStorage.getItem('profile-image');
		var cover_image = localStorage.getItem('cover-image');
		var token = localStorage.getItem('token');

		$http({
            method: 'POST',
            url: '/card/upload-card/',
            headers: {
    			'Content-Type': undefined,
                'Authorization': 'Token ' + token,
            },
            data: {
                title: $scope.title,
                desc: $scope.desc,
                status: $scope.status,
                profile_name: $scope.profile_name,
                card_image: card_image,
                cover_image:cover_image,
                profile_image:profile_image,
                fb_link: $scope.fb_link,
                insta_link: $scope.insta_link,
                twitter_link: $scope.twitter_link,
                youtube_link: $scope.youtube_link,
                youtube_video_1: $scope.youtube_video_1,
                youtube_video_2: $scope.youtube_video_2,
                youtube_video_3: $scope.youtube_video_3,
                youtube_playlist: $scope.youtube_playlist
            },
            transformRequest: function (data, headersGetter) {
                var formData = new FormData();
                angular.forEach(data, function (value, key) {
                    formData.append(key, value);
                });

                var headers = headersGetter();
                delete headers['Content-Type'];

                return formData;
            }
        })
        .success(function (data) {
			localStorage.removeItem('card_image');
			localStorage.removeItem('cover_image');
			localStorage.removeItem('profile_image');
			$window.location.href= '#/dashboard/';
        })
        .error(function (data, status) {

        });
	};
});

app.controller("LoginController", function($scope, $http, $window){

    localStorage.clear() //for log out
    $scope.login=function()
    {
        $scope.success =0;
        var user = {};
        user.email = $scope.email;
        user.password = $scope.pass;

        //Hit post request with required param and Url
        var url = '/authenticate-api/login-api/';
        $http.post(url, user).success(function(result){
            localStorage.setItem('token',result.token);
            localStorage.setItem('username', result.username);
            console.log(result);
            $window.location.href ='#/dashboard/';
            }).error(function(err) {
                $scope.msg = err.toString();
            });
    };
});


app.controller('SignUpController', function($scope, $http, $window){

    $scope.signUp = function()
    {
        var user = {}
        user.first_name = $scope.fname;
        user.last_name = $scope.lname;
        user.email = $scope.email;
        user.password = $scope.pwd;
        user.username = $scope.fname + ' ' +$scope.lname
        var url = '/authenticate-api/register-api/';
        $http.post(url, user).success(function(result){
            localStorage.setItem('token',result.token);
            localStorage.setItem('username', result.username);
            $window.location.href ='#/dashboard/';
        }).error(function(err) {
        $scope.msg = err.email[0];
       });
    };//signup function
});


app.controller('PageController', function($scope, $http){

    $scope.update_msg ='';
    $scope.username = localStorage.getItem('username');
    var token = localStorage.getItem('token');
    var config = {headers: {'Authorization': 'Token '+token} };

    $scope.changeTab=function(index)
    {
        $scope.selectedtabID=index;
        $scope.update_msg ='';
    };

    $scope.getPageData = function(val)
    {
        var url = '/pages/page-api/'+ val +'/';
        $http.get(url, config).success(function(result){
            console.log(result);
            $scope.title= result[0]['title'];
            $scope.description= result[0]['description'];
        }).error(function(err){
            console.log('error');
        });
    };

    $scope.updatePageData = function()
    {
        var page={};
        page.title = $scope.title;
        page.description = $scope.description;
        console.log(page);
        var url = '/pages/page-api/' +$scope.selectedtabID + '/';
        var config = {headers: {'Authorization': 'Token '+localStorage.getItem('token')} };

        $http.post(url, page, config).success(function(result){
            $scope.update_msg ='Successfully Updated!!';
            console.log(result);
        }).error(function(err){
            console.log(err);
        });
    };
});
