/*-------------------------------------------------------------------------------------------------------------------------------*/
/* 	THEME NAME 		: Square - Admin Template
* 	THEME VERSION	: 1.0.0
*	URL				: https://twitter.com/naztemplate
*	AUTHOR			: naztemplate.com
*	LAST UPDATE		: 10/11/2014
* 	COPYRIGHT		: Copyright 2014 All Rights Reserved. | by Square
*/
/*-------------------------------------------------------------------------------------------------------------------------------*/

(function() {

	'use strict';

	// basic
	$("#form").validate({
		highlight: function( label ) {
			$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function( label ) {
			$(label).closest('.form-group').removeClass('has-error');
			label.remove();
		},
		errorPlacement: function( error, element ) {
			var placement = element.closest('.input-group');
			if (!placement.get(0)) {
				placement = element;
			}
			if (error.text() !== '') {
				placement.after(error);
			}
		}
	});

	// validation summary
	var $summaryForm = $("#summary-form");
	$summaryForm.validate({
		errorContainer: $summaryForm.find( 'div.validation-message' ),
		errorLabelContainer: $summaryForm.find( 'div.validation-message ul' ),
		wrapper: "li"
	});

	// checkbox, radio and selects
	$("#chk-radios-form, #selects-form").each(function() {
		$(this).validate({
			highlight: function(element) {
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			},
			success: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		});
	});

}).apply( this, [ jQuery ]);